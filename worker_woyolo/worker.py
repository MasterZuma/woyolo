import json
import os
from logging import Logger, getLogger

import requests
from ultralytics import YOLO

from arkindex_worker.models import Element
from arkindex_worker.worker import ElementsWorker

logger: Logger = getLogger(__name__)


def create_element(element, label, coordonne, confidence):
    print("Requete ------------------------")
    session = requests.Session()

    Token = os.environ.get("ARKINDEX_API_TOKEN")
    url = os.environ.get("ARKINDEX_API_URL")

    try:
        url = url + "/api/v1/elements/create/"

        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Token {Token}",
        }

        payload = {
            "type": "object",  # required
            "name": label,  # required
            "corpus": f"{element.corpus.id}",
            "parent": f"{element.id}",
            "polygon": coordonne,
            "confidence": confidence,
        }
        # print(payload)
        # result = session.post(url, headers=headers, data=json.dumps(payload))
        session.post(url, headers=headers, data=json.dumps(payload))
        # print(result.json())

    except Exception as e:
        print(e)
    session.close()


def extract_box_info(boxes_data, element):
    # print("extraction ------------------------")
    """
    Extrait et imprime les information sur les boîtes détectées.

    Paramètres:
    boxes_data (list): Les données des boîtes détectées par le modèle YOLO.
    """
    labels = {
        0: "__background__",
        1: "person",
        2: "bicycle",
        3: "car",
        4: "motorcycle",
        5: "airplane",
        6: "bus",
        7: "train",
        8: "truck",
        9: "boat",
        10: "traffic light",
        11: "fire hydrant",
        12: "stop sign",
        13: "parking meter",
        14: "bench",
        15: "bird",
        16: "cat",
        17: "dog",
        18: "horse",
        19: "sheep",
        20: "cow",
        21: "elephant",
        22: "bear",
        23: "zebra",
        24: "giraffe",
        25: "backpack",
        26: "umbrella",
        27: "handbag",
        28: "tie",
        29: "suitcase",
        30: "frisbee",
        31: "skis",
        32: "snowboard",
        33: "sports ball",
        34: "kite",
        35: "baseball bat",
        36: "baseball glove",
        37: "skateboard",
        38: "surfboard",
        39: "tennis racket",
        40: "bottle",
        41: "wine glass",
        42: "cup",
        43: "fork",
        44: "knife",
        45: "spoon",
        46: "bowl",
        47: "banana",
        48: "apple",
        49: "sandwich",
        50: "orange",
        51: "broccoli",
        52: "carrot",
        53: "hot dog",
        54: "pizza",
        55: "donut",
        56: "cake",
        57: "chair",
        58: "couch",
        59: "potted plant",
        60: "bed",
        61: "dining table",
        62: "toilet",
        63: "tv",
        64: "laptop",
        65: "mouse",
        66: "remote",
        67: "keyboard",
        68: "cell phone",
        69: "microwave",
        70: "oven",
        71: "toaster",
        72: "sink",
        73: "refrigerator",
        74: "book",
        75: "clock",
        76: "vase",
        77: "scissors",
        78: "teddy bear",
        79: "hair drier",
        80: "toothbrush",
    }

    # Parcourir chaque boîte dans les données des boîtes
    for box in boxes_data:
        # Extraire les coordonnées et le label de la boîte
        x1, y1, x2, y2, confidence, class_id = box
        confidence = f"{confidence:.2f}"
        # print(confidence)
        label = labels[int(box[-1]) + 1]
        # print(label)
        x1 = int(x1.item())
        x2 = int(x2.item())
        y1 = int(y1.item())
        y2 = int(y2.item())
        coordonne = [[x1, y1], [x2, y1], [x2, y2], [x1, y2], [x1, y1]]

        create_element(element, label, coordonne, confidence)


class Demo(ElementsWorker):
    def process_element(self, element: Element) -> None:
        # print("Process de l'element ------------------------")
        logger.info(f"Demo processing element ({element.id})")
        # Chargement Yolo
        model = YOLO("yolov8n.pt")

        # Importer une image depuis une URL
        # response = requests.get("https://images.unsplash.com/photo-1600880292203-757bb62b4baf?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2070&q=80")
        # image = Image.open(BytesIO(response.content))
        # image = np.asarray(image)

        # ou bien en local
        image = element.open_image()

        # Detection
        results = model.predict(image)

        # Resultats
        # print(results[0].boxes.data)

        # Extraire et imprimer les information sur les boîtes
        extract_box_info(results[0].boxes.data, element)


def main() -> None:
    Demo(description="Test du modèle Yolo").run()


if __name__ == "__main__":
    main()
