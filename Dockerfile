FROM python:3.11

WORKDIR /src

COPY worker_woyolo worker_woyolo
COPY pyproject.toml ./

RUN pip install . --no-cache-dir

CMD ["python", "-m", "worker_woyolo.main"]
